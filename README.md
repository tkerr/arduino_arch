# arduino_arch #
A header file that attempts to determine the processor architecture in use and create an `ARDUINO_ARCH_xxxx` preprocessor definition.

If the architecure is not determined, then `ARDUINO_ARCH_UNKNOWN` is defined.

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License  
https://opensource.org/licenses/MIT
