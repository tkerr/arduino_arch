/*****************************************************************************
 * arduino_arch.h
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * Try to determine the Arduino processor architecture in use and create
 * an ARDUINO_ARCH_xxxx preprocessor definition.
 *
 * If the architecure is not determined, then ARDUINO_ARCH_UNKNOWN 
 * is defined.
 *
 * This is an evolving file and will be augmented as additional Arduino
 * processor architectures are used.
 */
#ifndef _ARDUINO_ARCH_H
#define _ARDUINO_ARCH_H

#define ARDUINO_ARCH_UNKNOWN

// AVR architectures
#ifdef __AVR__
    // #warning "__AVR__ is defined"
    #ifndef ARDUINO_ARCH_AVR
        #define ARDUINO_ARCH_AVR
    #endif
    #undef ARDUINO_ARCH_UNKNOWN
#endif  // __AVR__


// Mega AVR architectures
#ifdef __AVR_ATmega4809__
    // #warning "__AVR_ATmega4809__ is defined"
    #ifndef ARDUINO_ARCH_MEGAAVR
        #define ARDUINO_ARCH_MEGAAVR
    #endif
    #undef ARDUINO_ARCH_UNKNOWN
#endif  // __AVR_ATmega4809__


// SAMD51 architectures
#ifdef __SAMD51__
    // #warning "__SAMD51__ is defined"
    #ifndef ARDUINO_ARCH_SAMD
        #define ARDUINO_ARCH_SAMD
    #endif
    #undef ARDUINO_ARCH_UNKNOWN
#endif  // __SAMD51__

#ifdef ARDUINO_SAMD_ZERO
    // #warning "ARDUINO_SAMD_ZERO is defined"
    #ifndef ARDUINO_ARCH_SAMD
        #define ARDUINO_ARCH_SAMD
    #endif
    #undef ARDUINO_ARCH_UNKNOWN
#endif // ARDUINO_SAMD_ZERO


// PJRC 32-bit Teensy architectures
// See https://www.pjrc.com/teensy 

#ifdef ARDUINO_TEENSYLC
    // #warning "ARDUINO_TEENSYLC is defined"
    #ifndef ARDUINO_ARCH_TEENSY
        #define ARDUINO_ARCH_TEENSY
    #endif
    #ifndef ARDUINO_ARCH_TEENSYLC
        #define ARDUINO_ARCH_TEENSYLC
    #endif
    #undef ARDUINO_ARCH_UNKNOWN
#endif // ARDUINO_TEENSYLC

#ifdef ARDUINO_TEENSY35
    // #warning "ARDUINO_TEENSY35 is defined"
    #ifndef ARDUINO_ARCH_TEENSY
        #define ARDUINO_ARCH_TEENSY
    #endif
    #ifndef ARDUINO_ARCH_TEENSY35
        #define ARDUINO_ARCH_TEENSY35
    #endif
    #undef ARDUINO_ARCH_UNKNOWN
#endif // ARDUINO_TEENSY35

#ifdef ARDUINO_TEENSY36
    // #warning "ARDUINO_TEENSY36 is defined"
    #ifndef ARDUINO_ARCH_TEENSY
        #define ARDUINO_ARCH_TEENSY
    #endif
    #ifndef ARDUINO_ARCH_TEENSY36
        #define ARDUINO_ARCH_TEENSY36
    #endif
    #undef ARDUINO_ARCH_UNKNOWN
#endif // ARDUINO_TEENSY36

#ifdef ARDUINO_TEENSY40
    // #warning "ARDUINO_TEENSY40 is defined"
    #ifndef ARDUINO_ARCH_TEENSY
        #define ARDUINO_ARCH_TEENSY
    #endif
    #ifndef ARDUINO_ARCH_TEENSY40
        #define ARDUINO_ARCH_TEENSY40
    #endif
    #undef ARDUINO_ARCH_UNKNOWN
#endif // ARDUINO_TEENSY40

#ifdef ARDUINO_TEENSY41
    // #warning "ARDUINO_TEENSY41 is defined"
    #ifndef ARDUINO_ARCH_TEENSY
        #define ARDUINO_ARCH_TEENSY
    #endif
    #ifndef ARDUINO_ARCH_TEENSY41
        #define ARDUINO_ARCH_TEENSY41
    #endif
    #undef ARDUINO_ARCH_UNKNOWN
#endif // ARDUINO_TEENSY41


// Unknown architecture.
#ifdef ARDUINO_ARCH_UNKNOWN
    #warning "Unknown Arduino architecture!"
#endif  // ARDUINO_ARCH_UNKNOWN

#endif  // _ARDUINO_ARCH_H